package com.example.rest.service;

import com.example.rest.dao.PersonDB;
import com.example.rest.dao.PersonDao;
import com.example.rest.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    private PersonDao personDao;

    @Autowired
    public PersonService(@Qualifier("postgres") PersonDao personDao) {
        this.personDao = personDao;
    }

    public boolean addPerson(Person person) {
        return personDao.addPerson(person);
    }
}
