package com.example.rest.service;

import com.example.rest.dao.PersonDao;
import com.example.rest.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ContentService {
    private PersonDao personDao;

    @Autowired
    public ContentService(@Qualifier("postgres") PersonDao personDao) {
        this.personDao = personDao;
    }

    public Object getContent(String id) {
        Person person = personDao.getPerson(id);
        if (person.getRole().equals("admin")) {
            return personDao.getPeople();
        } else {
            return person;
        }
    }

//    public Object getPeople() {
//        return personDao.getPeople();
//    }


}
