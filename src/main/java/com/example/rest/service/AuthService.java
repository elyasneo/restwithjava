package com.example.rest.service;


import com.example.rest.model.Response;
import com.example.rest.dao.PersonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private PersonDao personDao;

    @Autowired
    public AuthService(@Qualifier("postgres") PersonDao personDao) {
        this.personDao = personDao;
    }

    public Response checkAuth(String username, String password) {
        return personDao.checkAuth(username, password);
    }
}
