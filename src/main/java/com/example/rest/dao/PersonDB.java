package com.example.rest.dao;

import com.example.rest.model.Response;
import com.example.rest.model.Person;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("postgres")
public class PersonDB implements PersonDao {
    private static final String SQL_DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/restdb";
    private static final String USER = "postgres";
    private static final String PASSWORD = "admin";

    @Override
    public boolean addPerson(Person person) {
        int responseCode = 0;
        String SQL = "INSERT INTO person (id,firstName,lastName,username,password,bcn, role , age) " +
                "VALUES(?, ?, ?,?,?,?,?,?)";
        try {
            Class.forName(SQL_DRIVER);
            Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
            PreparedStatement pstmt = con.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, person.getId().toString());
            pstmt.setString(2, person.getFirstName());
            pstmt.setString(3, person.getLastName());
            pstmt.setString(4, person.getUsername());
            pstmt.setString(5, person.getPassword());
            pstmt.setString(6, person.getBcn());
            pstmt.setString(7, person.getRole());
            pstmt.setInt(8, person.getAge());

            int affectedRows = pstmt.executeUpdate();

            if (affectedRows > 0) {
                responseCode = 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseCode = 0;
        }

        return responseCode == 1;
    }

    @Override
    public Person getPerson(String id) {
        Person person;
        String SQL_Qury = "select * from  person " +
                "where id = '" + id + "' ";
        try {
            Class.forName(SQL_DRIVER);
            Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL_Qury);
            if (rs.next()) {
                person = getPersonFromRS(rs);
                return person;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Person getPersonFromRS(ResultSet rs) throws SQLException {
        Person person;
        person = new Person(rs.getString("firstName"),
                rs.getString("lastName"),
                rs.getString("username"),
                rs.getString("password"),
                rs.getInt("age"),
                rs.getString("bcn"),
                rs.getString("role"));

        person.setId(UUID.fromString(rs.getString("id")));
        return person;
    }

    @Override
    public List<Person> getPeople() {
        List<Person> people = new ArrayList<>();
        String SQL_Qury = "select * from  person ";
        try {
            Class.forName(SQL_DRIVER);
            Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL_Qury);
            while (rs.next()) {
                Person person = getPersonFromRS(rs);
                people.add(person);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return people;
    }

    @Override
    public Response checkAuth(String username, String password) {
        String SQL_Qury = "select * from  person " +
                "where username = '" + username + "' ";

        try {
            Class.forName(SQL_DRIVER);
            Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SQL_Qury);
            while (rs.next()) {
                if (rs.getString("password").equals(password)) {
                    return new Response(200, rs.getString("id")); //success
                } else return new Response(401, "incorrect username or password"); //username or password incorrect
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Response(404, "user dose not exist"); //dose not exist
    }
}
