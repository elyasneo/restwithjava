package com.example.rest.dao;

import com.example.rest.model.Response;
import com.example.rest.model.Person;

import java.util.List;

public interface PersonDao {
    boolean addPerson(Person person);

    Person getPerson(String id);

    List<Person> getPeople();

    Response checkAuth(String username, String Password);
}
