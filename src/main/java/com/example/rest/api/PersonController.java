package com.example.rest.api;

import com.example.rest.model.Person;
import com.example.rest.model.Response;
import com.example.rest.service.PersonService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path = "/api/person", consumes = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RestController
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping
    public Response addPerson(@RequestBody Person person) {
        if (personService.addPerson(person)) {
            return new Response(200, person.getId().toString());
        }
        return new Response(401, "error");
    }
}
