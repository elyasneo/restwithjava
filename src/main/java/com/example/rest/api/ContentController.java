package com.example.rest.api;

import com.example.rest.service.ContentService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/content", consumes = MediaType.APPLICATION_JSON_VALUE)
public class ContentController {
    private ContentService contentService;

    public ContentController(ContentService contentService) {
        this.contentService = contentService;
    }

    @PostMapping
    public Object getContent(@RequestBody Map<String, String> body) {
        return contentService.getContent(body.get("id"));
    }
//
//    @GetMapping
//    public Object getPeople() {
//        return contentService.getPeople();
//    }
}
