package com.example.rest.api;

import com.example.rest.model.Person;
import com.example.rest.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/api/auth", consumes = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public Object checkAuth(@RequestBody Map<String, String> body) {
        return authService.checkAuth(body.get("username"),body.get("password"));
    }
}
