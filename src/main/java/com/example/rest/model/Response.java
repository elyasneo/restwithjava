package com.example.rest.model;

public class Response {
    public int code;
    public String msg;

    public Response(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
